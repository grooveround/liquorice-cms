import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import {PassportInterface} from './passport.interface';
import {TokenService} from './token.service';
import {environment} from '../../../environments/environment';

@Injectable()
export class AuthService {
    constructor(private http: HttpClient, private token: TokenService) {
    }

    login(username: string, password: string): Observable<boolean> {
        const params = {
            username: username,
            password: password,
            grant_type: `${environment.api.grant_type}`,
            client_id: `${environment.api.client_id}`,
            client_secret: `${environment.api.client_secret}`
        };

        return this.http.post<PassportInterface>(`${environment.api.url}/oauth/token`, params)
            .pipe(
                map(result => {
                    this.token.setToken(result);
                    return true;
                })
            );
    }

    logout() {
        this.token.removeToken();
    }

    public get loggedIn(): boolean {
        return (sessionStorage.getItem('access_token') !== null);
    }
}
