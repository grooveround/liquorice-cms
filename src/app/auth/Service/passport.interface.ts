export class PassportInterface {
    access_token: string;
    refresh_token: string;
    expires_in: string;
    token_type: string;
}