import {Injectable} from '@angular/core';
import {PassportInterface} from './passport.interface';

@Injectable()
export class TokenService {
    constructor() {
    }

    public getToken(): Promise<string> {
        return new Promise((resolve, reject) => {
            resolve(sessionStorage.getItem('access_token'));
        });
    }

    public setToken(token: PassportInterface) {
        sessionStorage.setItem('access_token', token.access_token);
        sessionStorage.setItem('refresh_token', token.refresh_token);
        sessionStorage.setItem('expires_in', token.expires_in);
        sessionStorage.setItem('token_type', token.token_type);
    }

    public removeToken() {
        sessionStorage.removeItem('access_token');
        sessionStorage.removeItem('refresh_token');
        sessionStorage.removeItem('expires_in');
        sessionStorage.removeItem('token_type');
    }

}