import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';


@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private accessTokenService: JwtHelperService) {
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (sessionStorage.getItem('access_token') !== 'undefined' && sessionStorage.getItem('access_token') !== null) {
            const token: string = sessionStorage.getItem('access_token');
            const isExpired = this.accessTokenService.isTokenExpired(token);

            if (isExpired) {
                sessionStorage.removeItem('access_token');
            } else {
                return true;
            }
        }

        this.router.navigate(['login']);
        return false;
    }
}
