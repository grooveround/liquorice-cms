import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from './auth/Guard/auth.guard';
import {ContentListComponent} from './contents/content-list.component';
import {LoginComponent} from './login/login.component';
import {ContentFormComponent} from './contents/content-form.component';

const routes: Routes = [
    {path: 'contents', component: ContentListComponent, canActivate: [AuthGuard]},
    {path: 'contents/edit/:id', component: ContentFormComponent, canActivate: [AuthGuard]},
    {path: 'contents/create', component: ContentFormComponent, canActivate: [AuthGuard]},
    {path: 'login', component: LoginComponent},
    // otherwise redirect to home
    {path: '**', redirectTo: 'contents'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
