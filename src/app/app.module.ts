import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {ContentListComponent} from './contents/content-list.component';
import {AuthService} from './auth/Service/auth.service';
import {AuthGuard} from './auth/Guard/auth.guard';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {JwtHelperService, JWT_OPTIONS} from '@auth0/angular-jwt';
import {TokenService} from './auth/Service/token.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatCardModule,
    MatDividerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
    MatTooltipModule,
    MatToolbarModule, MatDialogModule
} from '@angular/material';
import {FroalaEditorModule, FroalaViewModule} from 'angular-froala-wysiwyg';
import {ContentFormComponent} from './contents/content-form.component';
import {ConfirmDeleteDialogComponent} from './contents/Dialogs/confirm-delete-dialog.component';

export function jwtOptionsFactory(service) {
    return {
        tokenGetter: () => service.getToken()
    };
}

export const MY_DATE_FORMART = {
    parse: {
        dateInput: 'LL',
    },
    display: {
        dateInput: 'LL',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        ContentListComponent,
        ContentFormComponent,
        ConfirmDeleteDialogComponent
    ],
    entryComponents:[
        ConfirmDeleteDialogComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,

        MatTableModule,
        MatProgressSpinnerModule,
        MatPaginatorModule,
        MatSortModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        MatCardModule,
        MatDividerModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatTooltipModule,
        MatToolbarModule,
        MatDialogModule,
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot()
    ],
    providers: [
        AuthService,
        AuthGuard,
        JwtHelperService,
        TokenService,
        {
            provide: JWT_OPTIONS,
            useFactory: jwtOptionsFactory,
            deps: [TokenService]
        },
        {provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMART},
        {provide: MAT_DATE_LOCALE, useValue: 'en-ZA'}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
