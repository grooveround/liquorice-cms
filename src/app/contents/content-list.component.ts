import {Component, OnInit, ViewChild} from '@angular/core';
import {merge, Observable, of} from 'rxjs';
import {Content} from './Model/content';
import {ContentService} from './Service/content.service';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {map, startWith, switchMap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {ContentCollectionParams} from './Service/ContentCollectionParams';
import {DataItem} from './Transformers/DataItem';
import {ConfirmDeleteDialogComponent} from './Dialogs/confirm-delete-dialog.component';

@Component({
    selector: 'app-todo-list',
    templateUrl: './content-list.component.html',
    styleUrls: ['./content-list.component.css']
})
export class ContentListComponent implements OnInit {
    confirmDelete: boolean;
    filters: Content = new Content();
    collection$: Observable<DataItem<Content>[]>;
    pagination$: Observable<any>;

    displayedColumns: string[] = ['id', 'title', 'status', 'createdAt', 'actions'];

    resultsLength = 0;
    isLoadingResults = true;
    isRateLimitReached = false;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private router: Router, private service: ContentService, public dialog: MatDialog) {
    }

    ngOnInit() {

        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe((event) => {
            this.paginator.pageIndex = 0;
        });

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    return this.service.getCollection(new ContentCollectionParams(this.paginator.pageIndex, {
                        orderBy: this.sort.active,
                        direction: this.sort.direction
                    }, this.filters));
                }),
                map(result => {
                    this.collection$ = of(result.data);
                    this.pagination$ = of(result.meta.pagination);

                    // Flip flag to show that loading has finished.
                    this.isLoadingResults = false;
                    this.isRateLimitReached = false;
                    this.resultsLength = result.meta.pagination.total;
                })
            ).subscribe(data => console.log(data));
    }

    onFilter() {
        console.log('filters', this.filters);
        this.paginator.pageIndex = 0;
        return this.service.getCollection(new ContentCollectionParams(this.paginator.pageIndex, {
            orderBy: this.sort.active,
            direction: this.sort.direction
        }, this.filters)).subscribe(result => {
            this.collection$ = of(result.data);
            this.pagination$ = of(result.meta.pagination);

            // Flip flag to show that loading has finished.
            this.isLoadingResults = false;
            this.isRateLimitReached = false;
            this.resultsLength = result.meta.pagination.total;
            console.log(result);
        });
    }

    onClearFilter() {
        this.filters = new Content();
        this.paginator.pageIndex = 0;
        return this.service.getCollection(new ContentCollectionParams(this.paginator.pageIndex, {
            orderBy: this.sort.active,
            direction: this.sort.direction
        }, this.filters)).subscribe(result => {
            this.collection$ = of(result.data);
            this.pagination$ = of(result.meta.pagination);

            // Flip flag to show that loading has finished.
            this.isLoadingResults = false;
            this.isRateLimitReached = false;
            this.resultsLength = result.meta.pagination.total;
            console.log(result);
        });
    }

    openDialog(id: number) {
        console.log('ID', id);
        const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
            width: '250px',
            data: {confirmDelete: this.confirmDelete, id: id}
        });

        dialogRef.afterClosed().subscribe(result => {
            // this.confirmDelete = result;
            if (result.confirmDelete) {
                console.log('The dialog was closed', result);
                this.service.delete(result.id).subscribe(() => {
                    this.service.getCollection(new ContentCollectionParams(this.paginator.pageIndex, {
                        orderBy: this.sort.active,
                        direction: this.sort.direction
                    }, this.filters)).subscribe(response => {
                        this.collection$ = of(response.data);
                        this.pagination$ = of(response.meta.pagination);

                        // Flip flag to show that loading has finished.
                        this.isLoadingResults = false;
                        this.isRateLimitReached = false;
                        this.resultsLength = response.meta.pagination.total;
                    });
                });
            }
            return false;
        });
    }
}
