import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ContentService} from './Service/content.service';
import {Content} from './Model/content';

@Component({
    selector: 'app-content-form',
    templateUrl: './content-form.component.html',
    styleUrls: ['./content-form.component.css']
})
export class ContentFormComponent implements OnInit {
    public model: Content = new Content();
    public action = 'Create';
    public id: number;

    constructor(private route: ActivatedRoute, private router: Router, private service: ContentService) {
    }

    ngOnInit() {
        this.id = +this.route.snapshot.paramMap.get('id');

        if (this.id) {
            this.service.getItem(this.id).subscribe(response => {
                console.log(response.data);
                this.model = response.data.attributes;
                this.action = 'Update';
            });
        }
    }

    onSubmit() {
        if (this.id) {
            this.service.update(this.id, this.model).subscribe(response => {
                this.router.navigate(['/contents']);
            });
        } else {
            this.service.create(this.model).subscribe(response => {
                this.router.navigate(['/contents']);
            });
        }
    }

}
