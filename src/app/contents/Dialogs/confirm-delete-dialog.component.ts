import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
    selector: 'app-confirm-delete-dialog',
    templateUrl: 'confirm-delete-dialog.html'
})
export class ConfirmDeleteDialogComponent {
    constructor(
        public dialogRef: MatDialogRef<ConfirmDeleteDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { confirmDelete: boolean, id: number }) {
    }

    onNoClick(event): void {
        event.stopImmediatePropagation();
        this.data.confirmDelete = false;
        this.dialogRef.close(this.data);
    }

    onConfirmClick(event): void {
        event.stopImmediatePropagation();
        this.data.confirmDelete = true;
        this.dialogRef.close(this.data);
    }
}