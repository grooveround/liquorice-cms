export class Content {
  id: number;
  title: string;
  slug: string;
  description: string;
  html: string;
  status: string;
  createdAt: string;
  updatedAt: string;
}
