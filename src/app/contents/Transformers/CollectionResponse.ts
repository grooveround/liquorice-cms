import {DataItem} from './DataItem';

export interface CollectionResponse<T> {
    data: DataItem<T>[];
    meta: any;
}
