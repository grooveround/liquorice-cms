export interface DataItem<T> {
    type: string;
    id: string | number;
    attributes: T;
    links: {
        self: string;
    };
}
