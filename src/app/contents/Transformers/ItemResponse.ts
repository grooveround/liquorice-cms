import {DataItem} from './DataItem';

export interface ItemResponse<T> {
    data: DataItem<T>;
    meta: any;
}
