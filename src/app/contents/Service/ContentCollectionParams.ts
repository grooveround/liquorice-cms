import {Optional} from '@angular/core';
import {Content} from '../Model/content';

export class ContentCollectionParams {
    constructor(@Optional() public page?: number,
                @Optional() public sorting?: {
                    orderBy: string,
                    direction: string
                },
                @Optional() public filters?: Content) {
    }
}