import {Injectable, Optional} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Content} from '../Model/content';
import {environment} from '../../../environments/environment';
import {TokenService} from '../../auth/Service/token.service';
import {ContentCollectionParams} from './ContentCollectionParams';
import {CollectionResponse} from '../Transformers/CollectionResponse';
import {ItemResponse} from '../Transformers/ItemResponse';

@Injectable({
    providedIn: 'root'
})
export class ContentService {
    protected accessToken: string;

    constructor(private http: HttpClient, private token: TokenService) {
        token.getToken().then<string>(accessToken => this.accessToken = accessToken);
    }

    getCollection(args: ContentCollectionParams) {
        let params = new HttpParams();

        if (args.sorting.orderBy && args.sorting.direction) {
            params = params
                .set('orderBy', args.sorting.orderBy)
                .set('direction', args.sorting.direction);
        }

        if (args.page) {
            params = params.set('page', (++args.page).toString());
        }

        if (args.filters) {
            Object.getOwnPropertyNames(args.filters).forEach(key => {
                if (key === 'createdAt' && args.filters[key]) {
                    const options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
                    const date: Date = new Date(Date.parse(args.filters[key]));
                    const dateString = date.toLocaleDateString('en-ZA').split('/').join('-');

                    params = params.set(key, dateString);

                    console.log('CeatedAt', dateString);
                } else if (args.filters[key]) {
                    params = params.set(key, args.filters[key]);
                }
            });
        }

        console.log(params);
        return this.http.get<CollectionResponse<Content>>(`${environment.api.url}/api/contents`, {
            headers: new HttpHeaders({
                'Content-Type': 'application/vnd.api+json',
                'Authorization': `Bearer ${this.accessToken}`
            }),
            params: params
        });
    }

    getItem(id: number) {
        return this.http.get<ItemResponse<Content>>(`${environment.api.url}/api/contents/${id}`, {
            headers: new HttpHeaders({
                'Content-Type': 'application/vnd.api+json',
                'Authorization': `Bearer ${this.accessToken}`
            })
        });
    }

    create(model: Content) {
        return this.http.post<ItemResponse<Content>>(`${environment.api.url}/api/contents`, model, {
            headers: new HttpHeaders({
                'Content-Type': 'application/vnd.api+json',
                'Authorization': `Bearer ${this.accessToken}`
            })
        });
    }

    update(id: number, model: Content) {
        return this.http.put<ItemResponse<Content>>(`${environment.api.url}/api/contents/${id}`, model, {
            headers: new HttpHeaders({
                'Content-Type': 'application/vnd.api+json',
                'Authorization': `Bearer ${this.accessToken}`
            })
        });
    }

    delete(id: number) {
        return this.http.delete<ItemResponse<Content>>(`${environment.api.url}/api/contents/${id}`, {
            headers: new HttpHeaders({
                'Content-Type': 'application/vnd.api+json',
                'Authorization': `Bearer ${this.accessToken}`
            })
        });
    }
}
