import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {AuthService} from 'src/app/auth/Service/auth.service';
import {LoginModel} from './Model/login.model';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {
    @Input() public model: LoginModel = new LoginModel();
    public error: string;

    constructor(private auth: AuthService, private router: Router) {
    }

    public submit() {
        this.auth.login(this.model.username, this.model.password)
            .pipe(first())
            .subscribe(
                result => this.router.navigate(['todos']),
                err => this.error = 'Could not authenticate'
            );
    }
}
