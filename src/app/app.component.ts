import {Component} from '@angular/core';
import {AuthService} from './auth/Service/auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'app';

    constructor(private router: Router, private auth: AuthService) {
    }

    logout() {
        this.auth.logout();
        this.router.navigate(['/login']);
    }
}
